## log4j-infra

log4j-infra provides production level plugins for log4j2. The current version contains

* A configurable rolling file appender (InfraFileBasedAppender)'
* External stack traces (stored in external file to the log) to un-clutter the log.
* Configurable archiving policies based on time and/or size
 
see [change log](https://bitbucket.org/netfeverteam/log4j-infra/wiki/ChangeLog) for more details

## Usage (Quick Start)

Take a look at the source code in the [simple](../master/examples/simple/) example project.
    
to add the latest dependency

    <dependency>
        <groupId>me.yoram.log4j-infra</groupId>
        <artifactId>plugin</artifactId>
        <version>0.2.1</version>
        <scope>compile</scope>
    </dependency>

    <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-core</artifactId>
        <version>2.5</version>
        <scope>compile</scope>
    </dependency>
    
    <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-web</artifactId>
        <version>2.5</version>
        <scope>compile</scope>
    </dependency>

in your web.xml, configure the log4j2.xml path

    <context-param>
        <param-name>log4jConfiguration</param-name>
        <param-value>file://<FOLDER>/log4j2.xml</param-value>
    </context-param>

The following is a sample log4j2.xml you can configure the logger as follow

    <?xml version="1.0" encoding="UTF-8"?>
    <Configuration status="INFO" packages="me.yoram.commons.log4j">
        <Appenders>
            <Console name="Console" target="SYSTEM_OUT">
                <PatternLayout pattern="%d{ABSOLUTE} %-5p $${ctx:transactionId} [%c{1}] %m%n" />
            </Console>
                
            <InfraFileBasedAppender name="test" fileName="target/log/test.log" append="true">
                <PatternLayout pattern="%d{ABSOLUTE} %-5p [%c{1}] %m%n" />
                
                <InfraRollingPolicies>
                    <InfraRollingTimeBasedPolicy interval="86400000" />
                    <InfraRollingSizeBasedPolicy size="1" />
                </InfraRollingPolicies>
                
                <InfraArchivingPolicies deleteAfterArchiving="true">
                    <InfraArchivingZipPolicy pattern="target/log/backup-logs/swathly-yyyy-MM-dd-HH-mm-ss.zip" level="9" />
                </InfraArchivingPolicies>
            </InfraFileBasedAppender>
        </Appenders>
                
        <Loggers>
            <Root level="debug">
                <AppenderRef ref="Console" />
                <AppenderRef ref="test" />
            </Root>
        </Loggers>
    </Configuration>
    
## Appender explained

### InfraFileBasedAppender

This appender can have rolling policies and archiving policies.

    <InfraFileBasedAppender name="test" fileName="target/log/test.log" append="true">
        <PatternLayout ... >
                
        <InfraRollingPolicies ...>
                
        <InfraArchivingPolicies ...>
    </InfraFileBasedAppender>

#### InfraRollingPolicies

Rolling policies defines how the file is rolled (older contents erased from the log). You can configure it 

Time Based in milliseconds (86400000 ms is every 24h). 

    <InfraRollingPolicies>
        <InfraRollingTimeBasedPolicy interval="86400000" />
    </InfraRollingPolicies>

or size based in megabytes

    <InfraRollingPolicies>
        <InfraRollingSizeBasedPolicy size="1" />
    </InfraRollingPolicies>

In case many are specified, the first one to meet the criteria launch the file rolling

#### InfraArchivingPolicies

You can also archive the files before it is rolled. This policy will zip the file using the define pattern for filename
 
    <InfraArchivingPolicies deleteAfterArchiving="true">
        <InfraArchivingZipPolicy pattern="target/log/backup-logs/swathly-yyyy-MM-dd-HH-mm-ss.zip" level="9" />
    </InfraArchivingPolicies>

### Using snapshot

    <repositories>
        <repository>
            <id>log4j-infra-snapshot</id>
            <name>log4j-infra snapshot repository</name>
            <url>https://nexus.yoram.me/repository/maven-snapshots/</url>
        </repository>
    </repositories>
    
### License

The component is distributed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0).