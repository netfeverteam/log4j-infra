/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.log4j.runtime;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author theyoz
 * @since 23/05/2018
 */
public class ThreadSafeFile implements IThreadSafeFileOutputStream, Closeable {
    private final File f;
    private FileOutputStream out;
    private final boolean append;
    private final boolean immediateFlush;
    
    public ThreadSafeFile(final File f, final boolean append, final boolean immediateFlush) throws IOException {
        super();
        
        this.f = f;        
        this.append = append;
        this.immediateFlush = immediateFlush;
        resetFile();
    }
    
    private void resetFile() throws IOException {
        this.out = new FileOutputStream(f, this.append);        
    }
    
    @Override
    public void write(final byte[] buf) throws IOException {
        write(buf, 0, buf.length);
    }

    @Override
    public void write(final byte[] buf, final int offset, final int lenth) throws IOException {
        out.write(buf, offset, lenth);
        
        if (immediateFlush) {
            flush();
        }
    }
    
    @Override
    public void flush() throws IOException {
        this.out.flush();
    }
    
    public synchronized void safeRun(IThreadSafeFileRun tsfr) {
        tsfr.run(this);
    }
    
    @Override
    public File getFile() {
        return this.f;
    }

    @Override
    public FileOutputStream getOutputStream() {
        return out;
    }

    @Override
    public void close() throws IOException {
        out.close();
    }
}
