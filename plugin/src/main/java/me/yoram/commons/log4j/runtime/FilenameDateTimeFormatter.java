/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.log4j.runtime;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author theyoz
 * @since 23/05/2018
 */
public class FilenameDateTimeFormatter extends DateFormat {
    private static final long serialVersionUID = -6371307297814871057L;
    private static final Map<String, Integer> FIELDS = new HashMap<>();
    
    private final String template;
    private final FieldPosition fieldPosition; 

    static {
        FIELDS.put("yyyy", YEAR_FIELD);
        FIELDS.put("MM", MONTH_FIELD);
        FIELDS.put("dd", DAY_OF_WEEK_IN_MONTH_FIELD);
        FIELDS.put("HH", HOUR1_FIELD);
        FIELDS.put("mm", MINUTE_FIELD);
        FIELDS.put("ss", SECOND_FIELD);                
    }
    
    public FilenameDateTimeFormatter(final String template) {
        super();
        
        this.template = template;
        
        int begin = -1;
        int end = -1;
        int field = YEAR_FIELD;
        
        for (String key: FIELDS.keySet()) {
            int pos = template.indexOf(key);
            
            if (pos > begin) {
                begin = pos;
                end = begin + key.length();
                field = FIELDS.get(key);
            }            
        }
        fieldPosition = new FieldPosition(field);
        fieldPosition.setBeginIndex(begin);
        fieldPosition.setEndIndex(end);
    }
    
    @Override
    public StringBuffer format(Date date, StringBuffer toAppendTo,
            FieldPosition fieldPosition) {        
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        
        String s = this.template;
        s = s.replace("yyyy", "" + cal.get(Calendar.YEAR));
        s = s.replace("MM", leadingZero(cal.get(Calendar.MONTH) + 1));
        s = s.replace("dd", leadingZero(cal.get(Calendar.DAY_OF_MONTH)));
        s = s.replace("HH", leadingZero(cal.get(Calendar.HOUR)));
        s = s.replace("mm", leadingZero(cal.get(Calendar.MINUTE)));
        s = s.replace("ss", leadingZero(cal.get(Calendar.SECOND)));
        
        toAppendTo.insert(0, s);
        
        fieldPosition.setBeginIndex(this.fieldPosition.getBeginIndex());
        fieldPosition.setEndIndex(this.fieldPosition.getEndIndex());
        
        return toAppendTo;
    }

    @Override
    public Date parse(String source, ParsePosition pos) {
        throw new UnsupportedOperationException();
    }
    
    private String leadingZero(final String s) {
        if (s.length() == 1) {
            return "0" + s;
        } else {
            return s;
        }
    }

    private String leadingZero(final int i) {
        return leadingZero("" + i);
    }
}
