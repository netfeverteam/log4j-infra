/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.log4j;

import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;

import me.yoram.commons.log4j.runtime.IFileBasedAppender;
import me.yoram.commons.log4j.runtime.IRollingPolicy;

/**
 *
 * @author theyoz
 * @since 23/05/2018
 */
@Plugin(name = "InfraRollingSizeBasedPolicy", category = "Core", printObject = true)
public class InfraRollingSizeBasedPolicy implements IRollingPolicy {
    private final int size;
    private IFileBasedAppender appender = null;
    
    public InfraRollingSizeBasedPolicy(final int size) {
        super();
        
        this.size = size;
    }

    @Override
    public void initialize(IFileBasedAppender appender) {
        this.appender = appender;
    }

    @Override
    public boolean trigger() {
        if (appender == null) {
            return false;
        }
        
        return appender.getFile().length() / (1024 * 1024) >= size;
    }

    @PluginFactory
    public static InfraRollingSizeBasedPolicy createPolicy(
            @PluginAttribute(value = "size", defaultInt = 10) int size) {
        return new InfraRollingSizeBasedPolicy(size);
    }
}
