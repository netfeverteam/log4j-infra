/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.log4j.runtime;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author theyoz
 * @since 23/05/2018
 */
public interface IThreadSafeFileOutputStream {
    void write(final byte[] buf) throws IOException;
    void write(final byte[] buf, final int offset, final int lenth) throws IOException;    
    void flush() throws IOException;    
    File getFile();
    FileOutputStream getOutputStream();
}
