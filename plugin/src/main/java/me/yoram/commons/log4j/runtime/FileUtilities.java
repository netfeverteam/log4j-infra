/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.log4j.runtime;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;

/**
 *
 * @author theyoz
 * @since 23/05/2018
 */
public final class FileUtilities {
    private FileUtilities() {
    }

    static long getLastModifiedTime(final File f) {
        return f.lastModified();
    }

    static void setLastModifiedTime(final File f, long time) {
        f.setLastModified(time);
    }

    public static long getCreationTime(final File f) throws IOException {
        BasicFileAttributes attr = Files.readAttributes(f.toPath(), BasicFileAttributes.class);
        
        return attr.creationTime().toMillis();
    }
        
    public static void copy(final InputStream in, OutputStream out) throws IOException {
        byte[] buf = new byte[4096];
        int nread;
        
        while ((nread = in.read(buf)) > 0) {
            out.write(buf, 0, nread);
        }
    }
    
    public static void copy(final File source, final File dest) throws IOException {
        try (
                final InputStream in = new FileInputStream(source);
                final OutputStream out = new FileOutputStream(dest)
        ) {
            copy(in, out);
        }
    }
    
    static String getFileExtension(File f) {
        String name = f.getAbsolutePath();
        int pos = name.lastIndexOf('.');
        
        if (pos == -1) {
            return "";
        } else {
            return name.substring(pos);
        }
    }
    
    public static File findFileThatDoNotExists(File f) {
       if (!f.exists()) {
           return f;
       } else {
           String ext = getFileExtension(f);
           String fullname = f.getAbsolutePath();
           String name = fullname.substring(0, fullname.length() - ext.length());
           int i = 0;
           
           while (true) {
               File res = new File(name + "." + i++ + ext);
               
               if (!res.exists()) {
                   return res;
               }
           }
       }
        
       
    }

}
