/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.log4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.util.Date;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import me.yoram.commons.log4j.utils.StringUtils;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.status.StatusLogger;

import me.yoram.commons.log4j.runtime.FileUtilities;
import me.yoram.commons.log4j.runtime.FilenameDateTimeFormatter;
import me.yoram.commons.log4j.runtime.IArchivingPolicy;

/**
 *
 * @author theyoz
 * @since 23/05/2018
 */
@Plugin(name = "InfraArchivingZipPolicy", category = "Core", printObject = true)
public class InfraArchivingZipPolicy implements IArchivingPolicy {
    private static final org.apache.logging.log4j.Logger LOGGER = StatusLogger
            .getLogger();

    private final String pattern;
    private final int level;

    // running variables
    private File f = null;
    private OutputStream out = null;
    private ZipOutputStream zout = null;

    public InfraArchivingZipPolicy(final String pattern, final int level) {
        super();

        this.pattern = pattern;
        this.level = level;
    }

    @Override
    public boolean create() {
        File fPattern = new File(StringUtils.substitute(this.pattern));
        LOGGER.info("InfraArchivingZipPolicy: " + fPattern);
        System.out.println("Archiving Zip ");
        DateFormat fnf = new FilenameDateTimeFormatter(fPattern.getName());

        final File folder = fPattern.getParentFile() == null ?
                new File(fPattern.getAbsolutePath()).getParentFile() :
                fPattern.getParentFile();

        if (folder.isFile()) {
            LOGGER.error(
                    String.format(
                            "%s is not a folder, but the logger tries to write %s in it.",
                            folder.getAbsolutePath(),
                            fPattern.getName()));

            return false;
        }

        if (!folder.exists() && !folder.mkdirs()) {
            LOGGER.error(String.format("failed to create folder %s.", folder.getAbsolutePath()));

            return false;
        }

        fPattern = FileUtilities.findFileThatDoNotExists(new File(fPattern.getParentFile(), fnf.format(new Date())));
        
        try {
            out = new FileOutputStream(fPattern);
            zout = new ZipOutputStream(out);
            zout.setLevel(this.level);
            LOGGER.info(String.format("Archiving in %s", fPattern.getAbsolutePath()));
            return true;
        } catch (IOException e) {
            LOGGER.error(String.format("Error creating archive %s", fPattern.getAbsolutePath()), e);
            return false;
        }
    }

    @Override
    public boolean add(File f, String name) {
        try {
            zout.putNextEntry(new ZipEntry(name));
            copy(f, this.zout);
            return true;
        } catch (IOException e) {
            LOGGER.error(String.format("Error adding file to archive %s", this.f.getAbsolutePath(), e));
            return false;            
        }
    }

    @Override
    public void close() {
        try {
            zout.close();
        } catch (IOException e) {
            LOGGER.error(String.format("Error closing zip archive %s", f.getAbsolutePath(), e));            
        }

        try {
            out.close();
        } catch (IOException e) {
            LOGGER.error(String.format("Error closing archive %s", f.getAbsolutePath(), e));            
        }
        
        zout = null;
        out = null;
        f = null;
    }

    @PluginFactory
    public static InfraArchivingZipPolicy createPolicy(
            @PluginAttribute(value = "pattern", defaultString = "backup-yyyy-MM-dd-HH-mm-ss.zip") String pattern,
            @PluginAttribute(value = "level", defaultInt = 9) int level) {
        int mylevel = level;
        
        if (level < Deflater.BEST_SPEED) {
            LOGGER.warn(
                    String.format(
                            "Compression level for zip archiving policy is %d, it should be at minimum %d. Value set at minimum level.",
                            level, Deflater.BEST_SPEED));
            mylevel = Deflater.BEST_SPEED;
        }

        if (level > Deflater.BEST_COMPRESSION) {
            LOGGER.warn(
                    String.format(
                            "Compression level for zip archiving policy is %d, it should be at maximum %d. Value set at minimum level.",
                            level, Deflater.BEST_COMPRESSION));
            mylevel = Deflater.BEST_COMPRESSION;
        }
        
        return new InfraArchivingZipPolicy(pattern, mylevel);
    }
    
    private static void copy(final File f, final OutputStream out) throws IOException {
        InputStream in = new FileInputStream(f);
        
        try {
            byte[] buf = new byte[4096];
            int nread;
            
            while ((nread = in.read(buf)) > 0) {
                out.write(buf, 0, nread);
            }
        } finally {
            in.close();
        }
    }
}
