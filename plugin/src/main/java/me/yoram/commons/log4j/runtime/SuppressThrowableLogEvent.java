/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.log4j.runtime;

import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.ThreadContext.ContextStack;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.impl.ThrowableProxy;
import org.apache.logging.log4j.message.Message;

/**
 *
 * @author theyoz
 * @since 23/05/2018
 */
public class SuppressThrowableLogEvent implements LogEvent {
    private static final long serialVersionUID = -4733855571801378742L;

    private final LogEvent internal;
    
    public SuppressThrowableLogEvent(final LogEvent internal) {
        super();
        
        this.internal = internal;
    }

    @Override
    public Map<String, String> getContextMap() {
        return internal.getContextMap();
    }

    @Override
    public ContextStack getContextStack() {
        return internal.getContextStack();
    }

    @Override
    public Level getLevel() {
        return internal.getLevel();
    }

    @Override
    public String getLoggerName() {
        return internal.getLoggerName();
    }

    @Override
    public Marker getMarker() {
        return internal.getMarker();
    }

    @Override
    public Message getMessage() {
        return internal.getMessage();
    }

    @Override
    public long getTimeMillis() {
        return internal.getTimeMillis();
    }

    @Override
    public StackTraceElement getSource() {
        return internal.getSource();
    }

    @Override
    public String getThreadName() {
        return internal.getThreadName();
    }

    @Override
    public Throwable getThrown() {
        return null;
    }

    @Override
    public ThrowableProxy getThrownProxy() {
        return null;
    }

    @Override
    public boolean isEndOfBatch() {
        return internal.isEndOfBatch();
    }

    @Override
    public boolean isIncludeLocation() {
        return internal.isIncludeLocation();
    }

    @Override
    public void setEndOfBatch(boolean endOfBatch) {
        internal.setEndOfBatch(endOfBatch);
    }

    @Override
    public void setIncludeLocation(boolean locationRequired) {
        internal.setIncludeLocation(locationRequired);
    }

    @Override
    public String getLoggerFqcn() {
        return internal.getLoggerFqcn();
    }

    @Override
    public long getNanoTime() {
        return internal.getNanoTime();
    }
}
