/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.log4j;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.status.StatusLogger;

import me.yoram.commons.log4j.runtime.IArchivingPolicy;

/**
 *
 * @author theyoz
 * @since 23/05/2018
 */
@Plugin(name = "InfraArchivingPolicies", category = "Core", printObject = true)
public class InfraArchivingPolicies implements IArchivingPolicy {
    private static final Logger LOGGER = StatusLogger.getLogger();
    private final IArchivingPolicy[] policies;
    private final boolean deleteAfterArchiving;
    private final Set<File> archivedFiles = new HashSet<>();

    /**
     * store the current running archiving policies, those that did not returned
     * false on {@link IArchivingPolicy#create()} and
     * {@link IArchivingPolicy#add(File, String)}
     */
    private final Set<IArchivingPolicy> currentPolicies = new HashSet<>();

    private InfraArchivingPolicies(final IArchivingPolicy[] policies,
                                   final boolean deleteAfterArchiving) {
        super();

        this.policies = policies;
        this.deleteAfterArchiving = deleteAfterArchiving;
    }

    @Override
    public boolean create() {
        currentPolicies.clear();
        
        if (deleteAfterArchiving) {
            archivedFiles.clear();
        }

        for (final IArchivingPolicy policy : this.policies) {
            if (policy.create()) {
                currentPolicies.add(policy);
            }
        }

        return !currentPolicies.isEmpty();
    }

    @Override
    public boolean add(File f, String name) {
        final IArchivingPolicy[] loopedPolicies = currentPolicies.toArray(new IArchivingPolicy[0]);
                
        boolean allOk = true;
        for (final IArchivingPolicy policy : loopedPolicies) {
            if (!policy.add(f, name)) {               
                policy.close();
                currentPolicies.remove(policy);

                allOk = false;
                LOGGER.error(String.format("Archiving failed for %s, keeping file in folder.", f.getAbsolutePath()));
            }
        }

        if (deleteAfterArchiving && allOk) {
            archivedFiles.add(f);
        }
        
        return !currentPolicies.isEmpty();
    }

    @Override
    public void close() {
        for (final IArchivingPolicy policy : this.currentPolicies) {
            policy.close();
        }

        if (deleteAfterArchiving) {
            for (final File f : archivedFiles) {
                f.delete();
            }
        }
    }

    @PluginFactory
    public static InfraArchivingPolicies createPolicy(
            @PluginAttribute(value = "deleteAfterArchiving", defaultBoolean = true) boolean deleteAfterArchiving,
            @PluginElement("InfraArchivingPolicies") final IArchivingPolicy... policies) {
        return new InfraArchivingPolicies(policies, deleteAfterArchiving);
    }
}
