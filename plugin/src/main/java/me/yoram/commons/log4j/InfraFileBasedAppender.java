/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.log4j;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import me.yoram.commons.log4j.utils.StringUtils;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.impl.Log4jLogEvent;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.apache.logging.log4j.message.ParameterizedMessageFactory;

import me.yoram.commons.log4j.runtime.ArchiveRunnable;
import me.yoram.commons.log4j.runtime.BlankRollingPolicy;
import me.yoram.commons.log4j.runtime.FileUtilities;
import me.yoram.commons.log4j.runtime.IArchivingPolicy;
import me.yoram.commons.log4j.runtime.IArchivingRunnableEvents;
import me.yoram.commons.log4j.runtime.IFileBasedAppender;
import me.yoram.commons.log4j.runtime.IRollingPolicy;
import me.yoram.commons.log4j.runtime.IThreadSafeFileOutputStream;
import me.yoram.commons.log4j.runtime.IThreadSafeFileRun;
import me.yoram.commons.log4j.runtime.SuppressThrowableLogEvent;
import me.yoram.commons.log4j.runtime.ThreadSafeFile;

/**
 *
 * @author theyoz
 * @since 23/05/2018
 */
@Plugin(name = "InfraFileBasedAppender", category = "Core", elementType = "appender", printObject = true)
public class InfraFileBasedAppender extends AbstractAppender implements
        IFileBasedAppender, IArchivingRunnableEvents {
    private ThreadSafeFile tsf;
    private IRollingPolicy rollingPolicy;
    private final IArchivingPolicy archivingPolicy;
    private final boolean immediateFlush;
    private final Map<Thread, ArchiveRunnable> archivingThreads = new HashMap<>();
    private final Set<File> stackTraces = new HashSet<>();
    
    public InfraFileBasedAppender(final String name, final Filter filter,
                                  final Layout<? extends Serializable> layout, final File logFile,
                                  final boolean append, final boolean immediateFlush,
                                  final IRollingPolicy rollingPolicy,
                                  final IArchivingPolicy archivingPolicy) throws IOException {
        super(name, filter, layout);

        this.tsf = new ThreadSafeFile(logFile, append, immediateFlush);
        this.immediateFlush = immediateFlush;
        this.archivingPolicy = archivingPolicy;

        this.rollingPolicy = rollingPolicy;
        this.rollingPolicy.initialize(this);
    }

    private ThreadSafeFile silentCreateThreadSafeFile(final File f,
            final boolean append, final boolean immediateFlush) {
        try {
            return new ThreadSafeFile(f, append, immediateFlush);
        } catch (IOException e) {
            LOGGER.error(
                    "error creating log file, logging is stopped from this appender",
                    e);
            return null;
        }
    }

    @Override
    public void append(LogEvent event) {
        if (tsf == null) {
            return;
        }

        final LogEvent evt = event;        
        final Appender appender = this;

        tsf.safeRun(new IThreadSafeFileRun() {
            @Override
            public void run(IThreadSafeFileOutputStream out) {
                if (rollingPolicy.trigger()) {
                    try {
                        out.getOutputStream().close();

                        File backupFile = new File(tsf.getFile()
                                .getParentFile(), "log.bak");
                        backupFile = FileUtilities
                                .findFileThatDoNotExists(backupFile);

                        FileUtilities.copy(out.getFile(), backupFile);

                        Set<File> traces = new HashSet<>();
                        traces.addAll(stackTraces);

                        ArchiveRunnable runnable = new ArchiveRunnable(
                                backupFile, tsf.getFile().getName(), traces,
                                archivingPolicy, null);
                        Thread t = new Thread(runnable);
                        archivingThreads.put(t, runnable);
                        t.setDaemon(false);
                        t.start();

                        stackTraces.clear();
                        tsf = new ThreadSafeFile(out.getFile(), false,
                                immediateFlush);
                    } catch (IOException e) {
                        tsf = silentCreateThreadSafeFile(out.getFile(), true,
                                immediateFlush);
                        rollingPolicy = new InfraRollingPolicies(
                                new IRollingPolicy[0]);
                        LOGGER.error(
                                "Error rolling log file, disabling rolling policy",
                                e);
                    }
                }
            }
        });

        if (tsf == null) {
            return;
        }

        tsf.safeRun(new IThreadSafeFileRun() {
            @Override
            public void run(IThreadSafeFileOutputStream out) {
                try {
                    out.write(appender.getLayout().toByteArray(
                            new SuppressThrowableLogEvent(evt)));

                    if (evt.getThrown() != null) {                        
                        File f = File.createTempFile("wtstacktrace", ".trace",
                                tsf.getFile().getParentFile());
                        Throwable t = evt.getThrown();

                        t.printStackTrace(new PrintStream(f));

                        String stMessage = "Stack trace for exception in "
                                + f.getAbsolutePath();
                        
                        LogEvent event = new Log4jLogEvent(evt.getLoggerName(),
                                evt.getMarker(), evt.getLoggerFqcn(), evt
                                        .getLevel(),
                                ParameterizedMessageFactory.INSTANCE
                                        .newMessage(stMessage), null, evt
                                        .getContextMap(),
                                evt.getContextStack(), evt.getThreadName(),
                                null, System.currentTimeMillis());
                        
                        stMessage = new String(appender.getLayout()
                                .toByteArray(event));

                        out.write(stMessage.getBytes());
                        stackTraces.add(f);
                    }
                } catch (IOException e) {
                    LOGGER.error("Error writting stack trace to log file", e);
                }
            }
        });
    }

    @Override
    public File getFile() {
        if (tsf == null) {
            return null;
        } else {
            return tsf.getFile();
        }
    }

    @Override
    public void onFinish(ArchiveRunnable runnable) {
        synchronized (archivingThreads) {
            for (Entry<Thread, ArchiveRunnable> entry : archivingThreads
                    .entrySet()) {
                if (runnable == entry.getValue()) {
                    archivingThreads.remove(entry.getKey());

                    return;
                }
            }
        }
    }

    @Override
    public void waitArchiving() {
        Thread[] current;
        synchronized (archivingThreads) {
            current = archivingThreads.keySet().toArray(new Thread[0]);
        }

        for (Thread t : current) {
            if (t.isAlive()) {
                try {
                    t.join();
                } catch (InterruptedException e) {
                    LOGGER.info("Archiving thread interrrupted", e);
                }
            }
        }
    }

    @PluginFactory
    public static InfraFileBasedAppender createAppender(
            @PluginAttribute("name") final String name,
            @PluginElement("Filter") final Filter filter,
            @PluginElement("Layout") Layout<? extends Serializable> layout,
            @PluginAttribute("fileName") final String filename,
            @PluginAttribute(value = "append", defaultBoolean = true) final boolean append,
            @PluginAttribute(value = "immediateFlush", defaultBoolean = true) final boolean immediateFlush,
            @PluginElement("InfraRollingPolicies") final IRollingPolicy rollingPolicy,
            @PluginElement("InfraArchivingPolicies") final IArchivingPolicy archivingPolicy) {
        if (name == null) {
            LOGGER.error("No name provided for FileAppender");

            return null;
        }

        final File logFile = new File(StringUtils.substitute(filename));
        LOGGER.info("InfraFileBasedAppender: " + logFile.getAbsolutePath());

        final File folder = logFile.getParentFile() == null ?
                new File(logFile.getAbsolutePath()).getParentFile() :
                logFile.getParentFile();

        if (folder.isFile()) {
            LOGGER.error(
                    String.format(
                            "%s is not a folder, but the logger tries to write %s in it.",
                            folder.getAbsolutePath(),
                            logFile.getName()));

            return null;
        }

        if (!folder.exists() && !folder.mkdirs()) {
            LOGGER.error(String.format("failed to create folder %s.", folder.getAbsolutePath()));

            return null;
        }

        if (layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }

        IRollingPolicy myRollingPolicy = (rollingPolicy == null) ? new BlankRollingPolicy() : rollingPolicy;
        
        try {
            return new InfraFileBasedAppender(name, filter, layout, logFile,
                    append, immediateFlush, myRollingPolicy, archivingPolicy);
        } catch (IOException e) {
            LOGGER.error("Error creating InfraFileBasedAppender", e);
            return null;
        }
    }
}
