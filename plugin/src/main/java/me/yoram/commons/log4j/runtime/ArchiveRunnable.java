/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.log4j.runtime;

import java.io.File;
import java.util.Set;

/**
 *
 * @author theyoz
 * @since 23/05/2018
 */
public final class ArchiveRunnable implements Runnable{
    private final File logFile;
    private final String logFileName;
    private final IArchivingPolicy policy;
    private final Set<File> traceFiles;
    private final IArchivingRunnableEvents events;

    public ArchiveRunnable(final File logFile, final String logFileName,
            final Set<File> traceFiles, final IArchivingPolicy policy, final IArchivingRunnableEvents events) {
        super();

        this.logFile = logFile;
        this.logFileName = logFileName;

        this.policy = policy;
        this.traceFiles = traceFiles;
        
        this.events = events;
    }

    @Override
    public void run() {
        if (!policy.create()) {
            return;
        }

        if (!policy.add(logFile, logFileName)) {
            policy.close();
            return;
        }

        for (final File f : traceFiles) {
            String name;

            if (f.equals(logFile)) {
                name = logFileName;
            } else {
                name = "traces/" + f.getName();
            }

            if (!policy.add(f, name)) {
                policy.close();
                return;
            }
        }

        policy.close();
        
        onFinish();
    }

    private void onFinish() {
        if (events != null) {
            events.onFinish(this);
        }
    }
}
