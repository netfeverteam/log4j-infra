/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.log4j.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 16/09/18
 */
public class StringUtils {
    public static String substitute(String template) {
        //final String pattern = "\\$\\{([^}]*)\\}";
        final String pattern = "\\$\\{(.*?)}";
        final Pattern expr = Pattern.compile(pattern);
        final Matcher matcher = expr.matcher(template);

        while (matcher.find()) {
            final String key = matcher.group(1);

            String envValue = System.getProperty(key);

            if (envValue == null) {
                envValue = System.getenv(key);
            }

            if (envValue == null) {
                envValue = "";
            } else {
                envValue = envValue.replace("\\", "\\\\");
            }

            Pattern subexpr = Pattern.compile(Pattern.quote(matcher.group(0)));

            template = subexpr.matcher(template).replaceAll(envValue);
        }

        return template;
    }
}
