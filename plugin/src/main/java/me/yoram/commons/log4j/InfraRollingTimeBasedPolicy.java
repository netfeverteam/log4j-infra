/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.log4j;

import java.io.IOException;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.status.StatusLogger;

import me.yoram.commons.log4j.runtime.FileUtilities;
import me.yoram.commons.log4j.runtime.IFileBasedAppender;
import me.yoram.commons.log4j.runtime.IRollingPolicy;

/**
 *
 * @author theyoz
 * @since 23/05/2018
 */
@Plugin(name = "InfraRollingTimeBasedPolicy", category = "Core", printObject = true)
public class InfraRollingTimeBasedPolicy implements IRollingPolicy {
    private static final Logger LOGGER = StatusLogger.getLogger();
    
    private final long interval;
    private IFileBasedAppender appender;
    
    public InfraRollingTimeBasedPolicy(final long interval) {
        super();
        
        this.interval = interval;
    }
    
    @Override
    public boolean trigger() {
        if (appender == null) {
            return false;            
        }
        
        try {
            long creationTime = FileUtilities.getCreationTime(appender.getFile()); 
            long now = System.currentTimeMillis();
            
            return now - creationTime >= interval;
        } catch (IOException e) {
            LOGGER.error("error triggering time based logger", e);
            return false;            
        }
    }

    @Override
    public void initialize(IFileBasedAppender appender) {
        this.appender = appender;
    }

    @PluginFactory
    public static InfraRollingTimeBasedPolicy createPolicy(@PluginAttribute(value="interval", defaultLong = 1000 * 60 * 60 * 24) long sInterval) {
        return new InfraRollingTimeBasedPolicy(sInterval);
    }
}