/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.log4j;

import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;

import me.yoram.commons.log4j.runtime.IFileBasedAppender;
import me.yoram.commons.log4j.runtime.IRollingPolicy;

/**
 *
 * @author theyoz
 * @since 23/05/2018
 */
@Plugin(name = "InfraRollingPolicies", category = "Core", printObject = true)
public class InfraRollingPolicies implements IRollingPolicy {
    private final IRollingPolicy[] policies; 
    
    public InfraRollingPolicies(final IRollingPolicy... policies) {
        super();
        
        this.policies = policies;
    }
    
    @Override
    public void initialize(IFileBasedAppender appender) {
        for (final IRollingPolicy o: this.policies) {
            o.initialize(appender);
        }
    }

    @Override
    public boolean trigger() {
        for (IRollingPolicy policy: policies) {
            if (policy.trigger()) {
                return true;
            }
        }
        return false;
    }    

    @PluginFactory
    public static InfraRollingPolicies createPolicy(
            @PluginElement("InfraRollingPolicies") final IRollingPolicy... policies) {
        return new InfraRollingPolicies(policies);
    }
}
