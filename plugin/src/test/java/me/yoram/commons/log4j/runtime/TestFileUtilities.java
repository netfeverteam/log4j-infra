package me.yoram.commons.log4j.runtime;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import me.yoram.commons.log4j.Utils;

public class TestFileUtilities {
    @Test
    public void testGetCreationTime() throws Exception {
        DateFormat fmt = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SS");
        long beforeCreation = System.currentTimeMillis() - 1000;
        File f = File.createTempFile("TestFileUtilities", ".tmp");
        long afterCreation = System.currentTimeMillis() + 1000;

        try {
            long reportedTime = FileUtilities.getCreationTime(f);

            assert (reportedTime >= beforeCreation)
                    && (reportedTime <= afterCreation) : String
                    .format("The creation date was %s but should have been between %s and %s. In millisecond %d <= %d <= %d",
                            fmt.format(new Date(reportedTime)),
                            fmt.format(new Date(beforeCreation)),
                            fmt.format(new Date(afterCreation)),
                            beforeCreation, reportedTime, afterCreation);
        } finally {
            f.delete();
        }
    }
    
    @Test
    public void testGetLasModified() throws Exception {
        DateFormat fmt = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SS");
        File f = File.createTempFile("TestFileUtilities", ".tmp");

        try {
            OutputStream out = new FileOutputStream(f);

            try {
                out.write(12);
                out.flush();
                out.close();
                long lastModified = FileUtilities.getLastModifiedTime(f);
                
                while (System.currentTimeMillis() < lastModified + 2000) {
                    Utils.sleep(1000);
                }

                out = new FileOutputStream(f);
                out.write(12);
                out.flush();
                out.close();
                long lastModified2 = FileUtilities.getLastModifiedTime(f);

                assert (lastModified < lastModified2) : String
                        .format("The last modified date %s should be greater than %s. In millisecond %d <= %d",
                                fmt.format(new Date(lastModified2)),
                                fmt.format(new Date(lastModified)),
                                lastModified, lastModified2);
            } finally {
                out.close();
            }

        } finally {
            f.delete();
        }
    }

    @Test
    public void testSetLasModified() throws Exception {
        DateFormat fmt = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SS");
        File f = File.createTempFile("TestFileUtilities", ".tmp");

        try {
            long expected = FileUtilities.getLastModifiedTime(f) + 10000;
            FileUtilities.setLastModifiedTime(f, expected);
            long lastModified = FileUtilities.getLastModifiedTime(f);

            assert (lastModified == expected) : String
                    .format("The last modified date should be %s but was %s instead",
                            fmt.format(new Date(expected)),
                            fmt.format(new Date(lastModified)));
        } finally {
            f.delete();
        }
    }
    
    @DataProvider
    public Object[][] dpGetFileExtension() {
        return new Object[][] {
                { new File("test.log"), ".log" },
                { new File("test."), "." },
                { new File("test"), "" }
        };
    }
    
    @Test(dataProvider = "dpGetFileExtension")
    public void testGetFileExtension(File f, String expected) throws Exception {
        String ext = FileUtilities.getFileExtension(f);
        
        assert ext.equals(expected) : String.format("Expected file ext [%s] but [%s] returned", expected, ext);
    }
    
    @Test
    public void testFindFileThatDoNotExists() throws Exception {
        File f = new File("target/tmp/test.log");
        f.getParentFile().mkdirs();
        f.createNewFile();
        
        File f2 = FileUtilities.findFileThatDoNotExists(f);
        assert f.getParentFile().getAbsolutePath().equals(f2.getParentFile().getAbsolutePath()) : "Root folder should be the same";
        assert !f.getAbsolutePath().equals(f2.getAbsolutePath()) : "File name should be the same";
        
        f.delete();
    }
}
