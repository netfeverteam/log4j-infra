package me.yoram.commons.log4j;

import java.io.File;
import java.nio.file.Files;
import java.text.DateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.testng.annotations.Test;

import me.yoram.commons.log4j.runtime.FilenameDateTimeFormatter;
import me.yoram.commons.log4j.runtime.IArchivingPolicy;

public class TestInfraArchivingZipPolicy {
    private static final String[] MODEL = new String[] { "file1.bin", "file2.bin", "subfolder/fileA.bin", "subfolder/fileB.bin"};

    /**
     * Create a folder with files with random data on the following model
     * 
     * <pre>
     * root
     *   file1.bin => 1024 chars
     *   file2.bin => 1024 chars
     *   subfolder
     *     fileA.bin => 1024 chars
     *     fileB.bin => 1024 chars
     * </pre>
     * 
     * @param folder
     * @throws Exception
     */
    private static void createModel(File folder) throws Exception {
        Utils.createModel(folder, MODEL);
    }

    private void archive(IArchivingPolicy archivePolicy, File rootFolder, int prefixLen) {
        String[] filenames = rootFolder.list();
        
        for (String filename: filenames) {
            File f = new File(rootFolder, filename);
            
            if (f.isDirectory()) {
                archive(archivePolicy, f, prefixLen);
            } else {
                String name = f.getAbsolutePath().replace('\\', '/').substring(prefixLen);
                archivePolicy.add(f, name);
            }
        }        
    }
    
    @Test
    public void testAlgorithm() throws Exception {
        File tmpModelFolder = Files.createTempDirectory("TestInfraArchivingZipPolicy").toFile();
        System.out.println("tmpModelFolder: " + tmpModelFolder.getAbsolutePath());
        createModel(tmpModelFolder);
        
        File tmpZipArchiveFolder = Files.createTempDirectory("TestInfraArchivingZipPolicy").toFile();
        System.out.println("tmpZipArchiveFolder: " + tmpZipArchiveFolder.getAbsolutePath());
        
        File zipArchive = new File(tmpZipArchiveFolder, "backup-yyyy-MM-dd-HH-mm-ss.zip");
        InfraArchivingZipPolicy archivePolicy = InfraArchivingZipPolicy.createPolicy(zipArchive.getAbsolutePath(), 9);
        assert archivePolicy.create() : "InfraArchivingZipPolicy.create() return false";
        
        try {
            archive(archivePolicy, tmpModelFolder, tmpModelFolder.getAbsolutePath().length() + 1);
        } finally {
            archivePolicy.close();
        }
        
        DateFormat sdf = new FilenameDateTimeFormatter("backup-yyyy-MM-dd-");
        String prefix = sdf.format(new Date());
        assert tmpZipArchiveFolder.list()[0].startsWith(prefix) : String.format("Archive in folder %s should start with prefix %s", tmpZipArchiveFolder.getAbsolutePath(), prefix);
        assert tmpZipArchiveFolder.list()[0].endsWith(".zip") : String.format("Archive in folder %s should end with .zip extension", tmpZipArchiveFolder.getAbsolutePath());
        
        zipArchive = new File(tmpZipArchiveFolder, tmpZipArchiveFolder.list()[0]);
                
        File unzipedArchiveFolder = Files.createTempDirectory("TestInfraArchivingZipPolicy").toFile();
        System.out.println("unzipedArchiveFolder: " + unzipedArchiveFolder.getAbsolutePath());
        Utils.unzip(zipArchive, unzipedArchiveFolder);
        
        Map<String, String> modelMd5 = Utils.md5Folder(tmpModelFolder);               
        Map<String, String> unzippedMd5 = Utils.md5Folder(unzipedArchiveFolder);
        
        for (Entry<String, String> entry: modelMd5.entrySet()) {
            String filename = entry.getKey();            
            assert unzippedMd5.containsKey(filename) : String.format("file name %s missing in unzipped folder", filename);
            
            String modelValueMd5 = entry.getValue();
            String unzippedValueMd5 = unzippedMd5.get(filename);
            assert modelValueMd5.equals(unzippedValueMd5) : String.format("file %s differs", filename);

            unzippedMd5.remove(filename);
        }
        
        assert unzippedMd5.size() == 0 : String.format("Extra file in unzipped folder: %s", unzippedMd5.keySet());
        
        FileUtils.deleteQuietly(tmpModelFolder);
        FileUtils.deleteQuietly(tmpZipArchiveFolder);
        FileUtils.deleteQuietly(unzipedArchiveFolder);
    }
}
