package me.yoram.commons.log4j;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import me.yoram.commons.log4j.runtime.IFileBasedAppender;

public class TestInfraFileBasedAppender {
    private static final Logger LOG = LogManager.getLogger(TestInfraFileBasedAppender.class);
    private static final String LN_STACKTRACE_PREFIX = "Stack trace for exception in";
    
    private static class LogFileInfo {
        public final Set<File> stackTraceFile = new HashSet<>();
        public final List<Integer> logLines = new ArrayList<>();
    }

    private LogFileInfo readLog(File f, File folder) throws IOException {
        LogFileInfo res = new LogFileInfo();
        
        InputStream in = new FileInputStream(f);
        try {
            BufferedReader bis = new BufferedReader(new InputStreamReader(in));
            
            try {
                String line;
                
                while ((line = bis.readLine()) != null) {
                    int pos = line.indexOf('[');
                    assert pos != -1 : "Log line incorrect (searching for '[': " + line;
                    
                    pos = line.indexOf(']', pos);
                    assert pos != -1 : "Log line incorrect (searching for ']': " + line;
                    
                    line = line.substring(pos + 1).trim();
                    
                    if (line.startsWith(LN_STACKTRACE_PREFIX)) {
                        File original = new File(line.substring(LN_STACKTRACE_PREFIX.length()).trim());
                        res.stackTraceFile.add(new File(folder, original.getName()));                        
                    } else {
                        res.logLines.add(new Integer(line));
                    }
                }
            } finally {
                bis.close();
            }
        } finally {
            in.close();
        }
        
        
        return res;
    }
    
    @Test
    public void testLogger() throws Exception {
        File backupFolder = new File("target/backup-log");
        FileUtils.deleteDirectory(backupFolder);
        backupFolder.mkdirs();
        
        // 21000 give us 2 archive and one log file with the remaining
        int numLogLine = 21000; 
        for (int round = 0; round < 2; round++) {
            for (int logCount = 0; logCount < numLogLine; logCount++) {
                if (logCount % 1000 == 0) {
                    LOG.error(logCount, new IOException("error no " + logCount));                
                } else {           
                    LOG.info(logCount);
                }
            }
        }
        
        org.apache.logging.log4j.core.Logger rootLogger = (org.apache.logging.log4j.core.Logger)LogManager.getRootLogger();
        IFileBasedAppender appender = (IFileBasedAppender)rootLogger.getAppenders().get("CustomAppender");
        
        System.out.println("Waiting for archiving to finish...");
        appender.waitArchiving();
        
        // keep an integer and a counter - it should end with a list of 2x21000
        Map<Integer, Integer> totalLineCount = new HashMap<>();

        File[] backupFiles = backupFolder.listFiles();
        assert backupFiles.length == 2 : String.format("At this point we should have 2 backup files but there is %d", backupFiles.length);
        
        for (File backup: backupFiles) {
            File tmpFolder = Files.createTempDirectory("TestInfraFileBasedAppender").toFile();
            System.out.println("unzipping backup " + backup.getName() + " in " + tmpFolder.getAbsolutePath());
            
            Utils.unzip(backup, tmpFolder);
            
            File[] tmp = new File(tmpFolder, "traces").listFiles();
            assert tmp != null : "stack trace folder not created";                       
            
            File logFile = new File(tmpFolder, appender.getFile().getName());
            assert logFile.isFile() : "The log file is missing";
            
            LogFileInfo info = readLog(logFile, new File(tmpFolder, "traces"));
            
            for (File trace: info.stackTraceFile) {
                assert trace.exists() : "The trace file " + trace.getAbsolutePath() + " was not found";
            }
            
            for (Integer i: info.logLines) {
                int count;
                
                if (totalLineCount.containsKey(i)) {
                    count = totalLineCount.get(i).intValue() + 1;
                } else {
                    count = 1;
                }
                
                totalLineCount.put(i, new Integer(count));
            }
            
            FileUtils.deleteQuietly(tmpFolder);
        }        
        
        if (appender.getFile().exists()) {
            LogFileInfo info = readLog(appender.getFile(), new File("traces"));
            
            for (Integer i: info.logLines) {
                int count;
                
                if (totalLineCount.containsKey(i)) {
                    count = totalLineCount.get(i).intValue() + 1;
                } else {
                    count = 1;
                }
                
                totalLineCount.put(i, new Integer(count));
            }
        }
        
        for (int i = 0; i < numLogLine; i++) {
            Integer key = new Integer(i);
            assert totalLineCount.containsKey(key) : "Log " + i + " is missing";
            
            int count = totalLineCount.get(key).intValue();
            assert count == 2 : "There should have been 2 entries for " + key.intValue() + " but " + count + " found";            
        }
    }
}
