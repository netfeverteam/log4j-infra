package me.yoram.commons.log4j.runtime;

import java.text.DateFormat;
import java.util.Calendar;

import org.testng.annotations.Test;

import me.yoram.commons.log4j.Utils;

public class TestFilenameDateTimeFormatter {
    @Test
    public void testFormat() {
        String template = "test-yyyy-MM-dd.HH-mm-ss";

        Calendar c = Calendar.getInstance();
        String expected = String.format("test-%d-%s-%s.%s-%s-%s",
                c.get(Calendar.YEAR),
                Utils.leadingZero(c.get(Calendar.MONTH) + 1),
                Utils.leadingZero(c.get(Calendar.DAY_OF_MONTH)),
                Utils.leadingZero(c.get(Calendar.HOUR)),
                Utils.leadingZero(c.get(Calendar.MINUTE)),
                Utils.leadingZero(c.get(Calendar.SECOND)));

        DateFormat df = new FilenameDateTimeFormatter(template);
        String res = df.format(c.getTime());
        
        assert res.equals(expected) : String.format("[%s] was expected but [%s] returned", expected, res);
    }
    
    @Test(expectedExceptions = {UnsupportedOperationException.class})
    public void testParse() throws Exception {
        new FilenameDateTimeFormatter("yyyy").parse("123");
    }
}
