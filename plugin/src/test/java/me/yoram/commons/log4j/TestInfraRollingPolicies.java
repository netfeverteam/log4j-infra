package me.yoram.commons.log4j;

import java.io.File;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import me.yoram.commons.log4j.runtime.IFileBasedAppender;
import me.yoram.commons.log4j.runtime.IRollingPolicy;

public class TestInfraRollingPolicies {
    private static class StandardFileBasedAppender implements IFileBasedAppender {
        private final File f;
        
        public StandardFileBasedAppender(final File f) {
            super();
            
            this.f = f;
        }
        
        @Override
        public File getFile() {
            return f;
        }

        @Override
        public void waitArchiving() {
        }
    }
        
    private File oneMegFile;
    private File timeOneHourFile;
        
    @BeforeClass
    public void startup() throws Exception {
        oneMegFile = File.createTempFile("TestInfraRollingPolicies", ".tmp");
        Utils.createRandomFile(oneMegFile, Utils.ONE_MB);
        
        timeOneHourFile = File.createTempFile("TestInfraRollingPolicies", ".tmp");
        Utils.createFile(timeOneHourFile, System.currentTimeMillis() - Utils.ONE_HOUR);
    }
    
    @AfterClass 
    public void tearDown() {
        oneMegFile.delete();
        timeOneHourFile.delete();
    }
    
    @DataProvider 
    public Object[][] dpTrigger() {
        IFileBasedAppender oneMegAppender = new StandardFileBasedAppender(oneMegFile);
        IFileBasedAppender oneHourAppender = new StandardFileBasedAppender(timeOneHourFile);
        
        IRollingPolicy oneMegSizeBased = InfraRollingSizeBasedPolicy.createPolicy(1);
        IRollingPolicy twoMegSizeBased = InfraRollingSizeBasedPolicy.createPolicy(2);
        IRollingPolicy afterOneHour = InfraRollingTimeBasedPolicy.createPolicy(Utils.ONE_HOUR);
        IRollingPolicy afterFiveHours = InfraRollingTimeBasedPolicy.createPolicy(5 * Utils.ONE_HOUR);
        
        return new Object[][] {
                {"1", oneMegSizeBased, oneMegAppender, true},
                {"2", twoMegSizeBased, oneMegAppender, false},
                {"3", afterOneHour, oneHourAppender, true},
                {"4", afterFiveHours, oneHourAppender, false},
                {"5", oneMegSizeBased, null, false},
                {"6", afterOneHour, null, false},
                {"7", InfraRollingPolicies.createPolicy(oneMegSizeBased, afterOneHour), oneMegAppender, true},
                {"8", InfraRollingPolicies.createPolicy(oneMegSizeBased, afterOneHour), oneHourAppender, true},
                {"9", InfraRollingPolicies.createPolicy(twoMegSizeBased, afterFiveHours), oneMegAppender, false},
                {"10", InfraRollingPolicies.createPolicy(twoMegSizeBased, afterFiveHours), oneHourAppender, false}
        };
    }
    
    @Test(dataProvider = "dpTrigger")
    public void testTrigger(String option, IRollingPolicy policy, IFileBasedAppender appender, boolean expectedValue) {
        policy.initialize(appender);
        
        boolean res = policy.trigger();
        
        assert res== expectedValue : String.format("expecting a %s here.", Boolean.toString(expectedValue));
    }
}
