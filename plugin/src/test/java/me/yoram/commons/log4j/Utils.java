package me.yoram.commons.log4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.FileTime;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;

public class Utils {
    public static final int ONE_KB = 1024;
    public static final int ONE_MB = 1024 * ONE_KB;
    public static final int ONE_SECOND = 1000;
    public static final int ONE_MINUTE = ONE_SECOND * 60;
    public static final int ONE_HOUR = ONE_MINUTE * 60;
    public static final int ONE_DAY = ONE_HOUR * 24;
    
    private static final SecureRandom RND = new SecureRandom();
    
    public static String leadingZero(final String s) {
        if (s.length() == 1) {
            return "0" + s;
        } else {
            return s;
        }
    }

    public static String leadingZero(final int i) {
        return leadingZero("" + i);
    }

    public static void createRandomFile(File f, int size) throws Exception {
        f.getParentFile().mkdirs();

        byte[] buf = new byte[size];
        RND.nextBytes(buf);
        Files.write(f.toPath(), buf, StandardOpenOption.CREATE);
    }
    
    public static void createModel(File rootFolder, String[] model) throws Exception {
        File[] fchildren = new File[model.length];
        for (int i = 0; i < fchildren.length; i++) {
            fchildren[i] = new File(rootFolder, model[i]);
        }
        
        for (File f: fchildren) {
            createRandomFile(f, 1024);            
        }
    }

    public static final void randomSleep() {
        sleep(RND.nextInt(500));
    }

    public static final void sleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {

        }
    }        

    public static final void createFile(File f, long creationDate) throws IOException {
        if (SystemUtils.IS_OS_LINUX) {
            createFileLinux(f, creationDate);
        } else if(SystemUtils.IS_OS_WINDOWS) {
        	createFileWindows(f, creationDate);
        } else {
            throw new UnsupportedOperationException("No algorith to set the file creation time on this OS coded. So do :)");
        }
    }
   
    private static void createFileWindows(File f, long creationDate) throws IOException {
		// TODO Auto-generated method stub
        BasicFileAttributeView attributes = Files.getFileAttributeView(Paths.get(f.getAbsolutePath()), BasicFileAttributeView.class);
        FileTime time = FileTime.fromMillis(creationDate);
        attributes.setTimes(time, time, time);
	}

	private static final void createFileLinux(File f, long creationDate) throws IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmm");
        String s = sdf.format(new Date(creationDate));
        String cmd = "touch -t " + s + " " + f.getAbsolutePath();
        Process p = Runtime.getRuntime().exec(cmd);
                
        try {
            p.waitFor();
        } catch (InterruptedException e) {
            throw new IOException(e.getMessage(), e);
        }        
        
        if (p.exitValue() != 0) {
            throw new IOException("Exit value for linux command " + cmd + " is " + p.exitValue());
        }
    }    
    
    public static void unzip(File archive, File folder) throws ZipException, IOException {
        ZipFile zipFile = new ZipFile(archive);
        
        try {
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                
                File f = new File(folder, entry.getName());
                
                if (entry.isDirectory()) {
                    f.mkdirs();
                } else {
                    File fparent = f.getParentFile();
                    if (!fparent.exists()) {
                        fparent.mkdirs();
                    }                

                    InputStream in = zipFile.getInputStream(entry);
                    try {
                        FileUtils.copyInputStreamToFile(in, f);
                    } finally {
                        in.close();
                    }
                }                        
            }             
        } finally {
            zipFile.close();
        }
    }
        
    public static void md5Folder(File folder, Map<String, String> map, int prefixLen) throws IOException {
        String[] filenames = folder.list();
        
        for (String filename: filenames) {
            File f = new File(folder, filename);
            
            if (f.isDirectory()) {
                md5Folder(f, map, prefixLen);
            } else {
                InputStream in = new FileInputStream(f);
                
                try {
                    String md5 = DigestUtils.md5Hex(in);
                    map.put(f.getAbsolutePath().substring(prefixLen), md5);
                } finally {
                    in.close();
                }
            }
          
        }
    }
    
    public static  Map<String, String> md5Folder(File folder) throws IOException {
        Map<String, String> res = new HashMap<>();
        
        md5Folder(folder, res, folder.getAbsolutePath().length() + 1);
        
        return res;
    }
    
}
