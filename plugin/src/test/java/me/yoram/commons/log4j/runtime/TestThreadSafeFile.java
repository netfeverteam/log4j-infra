package me.yoram.commons.log4j.runtime;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;

import me.yoram.commons.log4j.Utils;

public class TestThreadSafeFile {
    private static final int WRITE_ROUND = 3;

    private static class Runner implements IThreadSafeFileRun {
        private byte[] b;

        Runner(byte[] b) {
            super();

            this.b = b;
        }

        @Override
        public void run(IThreadSafeFileOutputStream out) {
            try {
                for (int i = 0; i < WRITE_ROUND; i++) {
                    out.write(b);

                    Utils.randomSleep();
                }

                out.write("\n".getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void testSafeRun() throws Exception {
        final byte[][] b = { "01234567".getBytes(), "abcdefgh".getBytes(),
                "ABCDEFGH".getBytes() };

        File f = File.createTempFile("TestThreadSafeFile", ".tmp");

        final ThreadSafeFile tsf = new ThreadSafeFile(f, false, true);

        try {
            Thread[] t = new Thread[b.length];

            for (int i = 0; i < b.length; i++) {
                final int j = i;
                t[i] = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        tsf.safeRun(new Runner(b[j]));
                    }
                });
            }

            for (Thread o : t) {
                o.start();
            }

            boolean finish;
            do {
                finish = true;

                for (Thread o : t) {
                    if (o.isAlive()) {
                        finish = false;
                        Utils.sleep(10);
                        break;
                    }
                }
            } while (!finish);
        } finally {
            tsf.close();
        }
        
        Set<String> controlSet = new HashSet<>();
        for (byte[] buf: b) {
            String part = new String(buf);
            String s = "";
            
            for (int i = 0; i < WRITE_ROUND; i++) {
                s = s + part;
            }
            controlSet.add(s);
        }
        
        InputStream in = new FileInputStream(f);
        
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            
            try {
                String s;
                
                while ((s = reader.readLine()) != null) {
                    int len = s.length();
                    int expectedLen = 8 * WRITE_ROUND;
                    assert len == expectedLen : String.format("Length of String should be %d, it is %d for [%s]", expectedLen, len, s);
                    assert controlSet.contains(s) : String.format("String [%s] not found in %s", s, f.getAbsolutePath());
                    controlSet.remove(s);
                }
                
                assert controlSet.size() == 0 : String.format("control set should be empty but is %s in %s", controlSet.toString(), f.getAbsolutePath());
            } finally {
                reader.close();
            }
        } finally {
            in.close();
        }
        
        f.delete();
    }
}
