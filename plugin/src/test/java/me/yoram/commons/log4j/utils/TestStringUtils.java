/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.log4j.utils;

import org.testng.annotations.Test;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 16/09/18
 */
public class TestStringUtils {
    @Test
    public void testSubstitute() {
        String res = StringUtils.substitute("${user.dir}/a");
        String expected = System.getProperty("user.dir") + "/a";

        assert expected.equals(res) : String.format("Expected %s but %s returned", expected, res);
    }
}
