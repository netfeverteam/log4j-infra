package me.yoram.commons.log4j.runtime;

import java.io.File;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.testng.annotations.Test;

import me.yoram.commons.log4j.Utils;

public class TestArchiveRunnable {
    private static final String[] MODEL = new String[] {"1.trace", "2.trace"};
    
    private static class TestArchivingPolicy implements IArchivingPolicy {
        private final Set<String> filenames = new HashSet<>();
        
        @Override
        public boolean create() {
            return true;
        }

        @Override
        public boolean add(File f, String name) {
            filenames.add(name);
            return true;
        }

        @Override
        public void close() {
        }
        
        
        public Set<String> getFilenames() {
            return filenames;
        }
    }
    
    private static class TestArchivingRunnableEvents implements IArchivingRunnableEvents {
        private Object expectedRunnable;
        private boolean called = false;
        private boolean expected = false;
        
        public boolean isCalled() {
            return this.called;
        }

        public boolean isExpected() {
            return this.expected;
        }
        
        public void setExpectedRunnable(Object expectedRunnable) {
            this.expectedRunnable = expectedRunnable;
        }

        @Override
        public void onFinish(ArchiveRunnable runnable) {
            called = true;
            expected = runnable == expectedRunnable;
        }
    }
    
    @Test
    public void testRun() throws Exception {
        File tmpLogsFolder = Files.createTempDirectory("TestArchiveRunnable").toFile();
        System.out.println("Logs created in " + tmpLogsFolder.getAbsolutePath());
        
        // create log file
        File logFile = new File(tmpLogsFolder, "log.log");
        FileUtils.write(logFile, "1234");
                
        // create backup log and stacktrace dummy
        Utils.createModel(tmpLogsFolder, MODEL);
        
        Set<File> traces = new HashSet<>();
        for (String s: MODEL) {
            traces.add(new File(tmpLogsFolder, s));
        }
        
        File backupLogFile = new File(tmpLogsFolder, "backup.log");
        FileUtils.copyFile(logFile, backupLogFile);        

        logFile.delete();
        FileUtils.write(logFile, "1234");
        
        // check we have 4 files
        assert tmpLogsFolder.list().length == 4 : "4 files expected at this point";
        
        TestArchivingPolicy policy = new TestArchivingPolicy();
        TestArchivingRunnableEvents events = new TestArchivingRunnableEvents(); 
        ArchiveRunnable o = new ArchiveRunnable(backupLogFile, "log.log", traces, policy, events);
        events.setExpectedRunnable(o);
        o.run();
                        
        assert events.isCalled() : "Event never called";
        assert events.isExpected() : "Event called but another runnable was given";
        
        Set<String> modelSet = new HashSet<>();
        for (String s: MODEL) {
            modelSet.add("traces/" + s);
        }        
        modelSet.add("log.log");
        
        Set<String> resultSet = policy.getFilenames();
        assert modelSet.size() == resultSet.size() : String.format("%s expected but %s returned", modelSet.toString(), resultSet.toString());
                
        for (String s: modelSet) {
            assert resultSet.contains(s) : String.format("File %s is missing from result", s);
        }
    }
}
