package me.yoram.commons.log4j.runtime;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.lang.reflect.Field;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.impl.ThrowableProxy;
import org.testng.annotations.Test;

public class TestSuppressThrowableLogEvent {
    private static void setField(Object instance, String fieldName, Object value) throws Exception {        
        Class<?> eventClass = Class.forName(instance.getClass().getName());
        Field field = eventClass.getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(instance, value);
    }

    @Test
    public void testGetterAndSetters() throws Exception {
        InputStream in = TestSuppressThrowableLogEvent.class.getResourceAsStream("/event.oos");
        
        ObjectInputStream ois = new ObjectInputStream(in);
        LogEvent event = (LogEvent)ois.readObject(); 
        ois.close();
        in.close();
        setField(event, "thrown", new IOException());
        setField(event, "thrownProxy", new ThrowableProxy(new IOException()));
        assert event.getThrown() != null : "getThrown() should not be null at startup";
        assert event.getThrownProxy() != null : "getThrownProxy() should not be null at startup";
        
        
        LogEvent suppressed = new SuppressThrowableLogEvent(event);

        assert event.getContextMap() == suppressed.getContextMap() : "getContextMap() differs";
        assert event.getContextStack() == suppressed.getContextStack() : "getContextStack() differs";
        assert event.getLoggerFqcn() == suppressed.getLoggerFqcn() : "getLoggerFqcn() differs";
        assert event.getLevel() == suppressed.getLevel() : "getLevel() differs";
        assert event.getLoggerName() == suppressed.getLoggerName() : "getLoggerName() differs";
        assert event.getMarker() == suppressed.getMarker() : "getMarker() differs";
        assert event.getTimeMillis() == suppressed.getTimeMillis() : "getTimeMillis() differs";
        assert event.getSource() == suppressed.getSource() : "getSource() differs";
        assert event.getThreadName() == suppressed.getThreadName() : "getThreadName() differs";
        assert suppressed.getThrown() == null : "getThrown() should be null with SuppressThrowableLogEvent";
        assert suppressed.getThrownProxy() == null : "getThrownProxy() should be null with SuppressThrowableLogEvent";
        assert event.isEndOfBatch() == suppressed.isEndOfBatch() : "isEndOfBatch() differs";

        for (int i = 0; i < 2; i++) {
            suppressed.setEndOfBatch(i == 0);
            suppressed.setIncludeLocation(i != 0);
            
            assert event.isIncludeLocation() == suppressed.isIncludeLocation() : "isIncludeLocation() differs";
            assert event.isEndOfBatch() == suppressed.isEndOfBatch() : "isEndOfBatch() differs";            
        }
    }
}
