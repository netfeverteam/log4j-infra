package me.yoram.commons.log4j;

import java.io.File;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import me.yoram.commons.log4j.runtime.IArchivingPolicy;

public class TestInfraArchivingPolicies {
    private static class StandardPolicy implements IArchivingPolicy {
        private int createCount = 0;
        private int addCount = 0;
        private int closeCount = 0;
        private final Map<String, File> addMap = new HashMap<>();
        private boolean createReturn = true;
        private boolean addReturn = true;

        @Override
        public boolean create() {
            createCount++;
            return createReturn;
        }

        @Override
        public boolean add(File f, String name) {
            if (addReturn) {
                addCount++;
                addMap.put(name, f);
            }
            
            return addReturn;
        }

        @Override
        public void close() {
            System.out.println("closing " + this.toString());
            closeCount++;
        }        
        
        public int getCreateCount() {
            return createCount;
        }

         public int getAddCount() {
            return addCount;
        }

        public int getCloseCount() {
            return closeCount;
        }

        public void setCreateReturn(boolean createReturn) {
            this.createReturn = createReturn;
        }

        public void setAddReturn(boolean addReturn) {
            this.addReturn = addReturn;
        }

        public Map<String, File> getAddMap() {
            return addMap;
        }
    }

    /*
     * test if delete after archiving (true and false)
     */

    /**
     * test if multiple policies are called (all working ok). 
     */
    @Test
    public void testIfMultiplePoliciesAreCalledAllOk() throws Exception {
        IArchivingPolicy[] policies = new IArchivingPolicy[] { new StandardPolicy(), new StandardPolicy() }; 
        IArchivingPolicy policy = InfraArchivingPolicies.createPolicy(false, policies);
        
        Map<String, File> controlMap = new HashMap<>();
        controlMap.put("aaa", new File("aaa"));
        controlMap.put("bbb", new File("aaa"));
        
        assert policy.create() : "policy create must return true at this point as all policies must succeed";
        
        for (Entry<String, File> entry: controlMap.entrySet()) {
            String name = entry.getKey();
            File f = entry.getValue();
            assert policy.add(f, name) : "All add on policies should succeed at this point.";            
        }

        policy.close();
        
        for (IArchivingPolicy p: policies) {
            StandardPolicy o = (StandardPolicy)p;
            assert o.getCreateCount() == 1 : String.format("There should have been only 1 create but %s returned", o.getCreateCount());
            assert o.getCloseCount() == 1 : String.format("There should have been only 1 close but %s returned", o.getCloseCount());
            assert o.getAddCount() == controlMap.size() : String.format("There should have been %d add call but only %d reported.", controlMap.size(), o.getAddCount());
            
            Map<String, File> map = o.getAddMap();
            assert map.size() == 2 : String.format("There should have been 2 files added but only %d was returned in %s", map.size(), map.toString());
            
            for (Entry<String, File> entry: controlMap.entrySet()) {
                String name = entry.getKey();
                File f = entry.getValue();
                
                assert map.containsKey(name) : String.format("%s is miising in %s", name, map.toString());
                assert map.get(name).equals(f) : String.format("Files %s expected but %s returned", f.getAbsolutePath(), map.get(name).getAbsolutePath());
            }            
        }
    }
    
    @DataProvider 
    public Object[][] dpCreateReturnsFalse() {
        IArchivingPolicy goodPolicy = new StandardPolicy();
        
        IArchivingPolicy badPolicy = new StandardPolicy();
        ((StandardPolicy)badPolicy).setCreateReturn(false);
        
        return new Object[][] { 
                {new IArchivingPolicy[] {goodPolicy, goodPolicy}, true},  
                {new IArchivingPolicy[] {badPolicy, goodPolicy}, true},  
                {new IArchivingPolicy[] {goodPolicy, badPolicy}, true},  
                {new IArchivingPolicy[] {badPolicy, badPolicy}, false}  
        };
    }
            
    @Test(dataProvider = "dpCreateReturnsFalse")
    public void testCreateReturnsFalse(IArchivingPolicy[] policies, boolean expectedResult) {
        IArchivingPolicy policy = InfraArchivingPolicies.createPolicy(false, policies);

        assert policy.create() == expectedResult : String.format("%s expected here.", Boolean.toString(expectedResult));
    }

    @DataProvider 
    public Object[][] dpAddReturnsFalse() {
        IArchivingPolicy goodPolicy = new StandardPolicy();
        
        IArchivingPolicy badPolicy = new StandardPolicy();
        ((StandardPolicy)badPolicy).setAddReturn(false);
        
        return new Object[][] { 
                {new IArchivingPolicy[] {goodPolicy, goodPolicy}, true},  
                {new IArchivingPolicy[] {badPolicy, goodPolicy}, true},  
                {new IArchivingPolicy[] {goodPolicy, badPolicy}, true},  
                {new IArchivingPolicy[] {badPolicy, badPolicy}, false}  
        };
    }
            
    @Test(dataProvider = "dpAddReturnsFalse")
    public void testAddReturnsFalse(IArchivingPolicy[] policies, boolean expectedResult) {
        IArchivingPolicy policy = InfraArchivingPolicies.createPolicy(false, policies);

        policy.create();
        assert policy.add(new File("aaa"), "aaa") == expectedResult : String.format("%s expected here.", Boolean.toString(expectedResult));
    }
    
    @Test
    public void testIfPolicyIsDroppedOnCreateFalse() {
        IArchivingPolicy[] policies = new IArchivingPolicy[] { new StandardPolicy(), new StandardPolicy() }; 
        ((StandardPolicy)policies[0]).setCreateReturn(false);
        
        IArchivingPolicy policy = InfraArchivingPolicies.createPolicy(false, policies);
        
        Map<String, File> controlMap = new HashMap<>();
        controlMap.put("aaa", new File("aaa"));
        controlMap.put("bbb", new File("aaa"));
        
        assert policy.create() : "policy create must return true at this point as all policies must succeed";
        
        for (Entry<String, File> entry: controlMap.entrySet()) {
            String name = entry.getKey();
            File f = entry.getValue();
            assert policy.add(f, name) : "All add on policies should succeed at this point.";            
        }

        policy.close();
        
        for (int i = 0; i < policies.length; i++) {
            StandardPolicy o = (StandardPolicy)policies[i];
            assert o.getCreateCount() == 1 : String.format("There should have been only 1 create but %d returned", o.getCreateCount());
            
            if (i == 0) {
                assert o.getCloseCount() == 0 : String.format("There should have been only 0 close but %d returned", o.getCloseCount());
                assert o.getAddCount() == 0 : String.format("addcount should be 0 in this policy as it returned false on create but %d found", o.getAddCount());                                
            } else {
                assert o.getCloseCount() == 1 : String.format("There should have been only 1 close but %d returned", o.getCloseCount());
                assert o.getAddCount() == controlMap.size() : String.format("There should have been %d add call but only %d reported.", controlMap.size(), o.getAddCount());                
            }            
        }        
    }

    @Test
    public void testIfPolicyIsDroppedOnAddFalse() {
        IArchivingPolicy[] policies = new IArchivingPolicy[] { new StandardPolicy(), new StandardPolicy() }; 
        ((StandardPolicy)policies[0]).setAddReturn(false);
        
        IArchivingPolicy policy = InfraArchivingPolicies.createPolicy(false, policies);
        
        Map<String, File> controlMap = new HashMap<>();
        controlMap.put("aaa", new File("aaa"));
        controlMap.put("bbb", new File("aaa"));
        
        assert policy.create() : "policy create must return true at this point as all policies must succeed";
        
        for (Entry<String, File> entry: controlMap.entrySet()) {
            String name = entry.getKey();
            File f = entry.getValue();
            assert policy.add(f, name) : "All add on policies should succeed at this point.";            
        }

        policy.close();
        
        for (int i = 0; i < policies.length; i++) {
            StandardPolicy o = (StandardPolicy)policies[i];
            assert o.getCreateCount() == 1 : String.format("There should have been only 1 create but %d returned where i = %d", o.getCreateCount(), i);
            assert o.getCloseCount() == 1 : String.format("There should have been only 1 close but %d returned where i = %d", o.getCloseCount(), i);
            
            if (i == 0) {
                assert o.getAddCount() == 0 : String.format("addcount should be 0 in this policy as it returned false on add but %d found", o.getAddCount());                                
            } else {
                assert o.getAddCount() == controlMap.size() : String.format("There should have been %d add call but only %d reported.", controlMap.size(), o.getAddCount());                
            }            
        }        
    }
          
    @DataProvider
    public Object[][] dpDeleteAfterArchiving() {
        String[] model = new String[] {"a.txt", "b.txt", "c.txt"};
        
        return new Object[][] {
                {model, false, -1, model},
                {model, false, 1, model},
                {model, true, -1, new String[0]},
                {model, true, 0, model},
                {model, true, 1, new String[] {"b.txt", "c.txt"}},
                {model, true, 2, new String[] {"c.txt"}}
        };
    }
    
    @Test(dataProvider = "dpDeleteAfterArchiving")
    public void testDeleteAfterArchiving(String[] model, boolean deleteAfterArchiving, int failedOnIndex, String[] remainingModel) throws Exception {
        IArchivingPolicy[] policies = new IArchivingPolicy[] { new StandardPolicy() };         
        IArchivingPolicy policy = InfraArchivingPolicies.createPolicy(deleteAfterArchiving, policies);
        
        File tmpArchiveFolder = Files.createTempDirectory("TestInfraArchivingPolicies").toFile();
        System.out.println("tmpZipArchiveFolder: " + tmpArchiveFolder.getAbsolutePath());
        
        for (String s: model) {
            File f = new File(tmpArchiveFolder, s);
            Utils.createRandomFile(f, 1024);
        }
        
        assert policy.create();
        
        for (int i = 0; i < model.length; i++) {            
            File f = new File(tmpArchiveFolder, model[i]);
            
            if (i == failedOnIndex) {
                ((StandardPolicy)policies[0]).setAddReturn(false);
            }
            
            if (!policy.add(f, model[1])) {
                break; 
            }
        }
        
        policy.close();
        
        Set<String> remainFilenames = new HashSet<>(Arrays.asList(tmpArchiveFolder.list()));
        Set<String> remainModel = new HashSet<>(Arrays.asList(remainingModel));
        
        assert remainFilenames.size() == remainModel.size() : String.format("Files expected in folder %s were %s but %s found.", tmpArchiveFolder.getAbsolutePath(), remainModel.toString(), remainFilenames.toString());
        
        FileUtils.deleteQuietly(tmpArchiveFolder);
    }
}
