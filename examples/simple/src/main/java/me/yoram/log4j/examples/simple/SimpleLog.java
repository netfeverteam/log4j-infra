/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.log4j.examples.simple;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import java.util.UUID;

/**
 *
 * @author theyoz
 * @since 23/05/2018
 */
public class SimpleLog {
    private static final Logger LOG = LogManager.getLogger(SimpleLog.class);

    public static void main(String[] args) {
        ThreadContext.put("transactionId", "xxx");

        for (int i = 0; i < 40000; i++) {
            LOG.info(UUID.randomUUID().toString());
        }
    }
}
